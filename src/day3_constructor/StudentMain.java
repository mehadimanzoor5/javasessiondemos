package day3_constructor;

public class StudentMain {

	public static void main(String[] args) {
		StudentDetails studetails= new StudentDetails(110, "Manzoor");
		studetails.display();
		StudentDetails studetails1= new StudentDetails(114, "Mehadi");
		studetails1.display();
		StudentDetails stuFeeDetails= new StudentDetails(114, 50000);
		stuFeeDetails.displayFee();
	}

}
