package day3_constructor;

public class StudentDetails {

	int stuId;
	String stuName;
	int stuFee;
	
	public StudentDetails(int id, String name) {
		stuId=id;
		stuName=name;
	}
	
	public StudentDetails(int id, int fee) {
		stuId=id;
		stuFee=fee;
	}
	void display() {
		System.out.println(stuId+" "+stuName);
	}
	
	void displayFee() {
		System.out.println(stuId+" "+stuFee);
	}
}
