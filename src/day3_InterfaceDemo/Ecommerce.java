package day3_InterfaceDemo;

public interface Ecommerce {

	public abstract String memberDetails(String memberName);
	
	public abstract int purchaseOrder(int noOfOrders);
}
