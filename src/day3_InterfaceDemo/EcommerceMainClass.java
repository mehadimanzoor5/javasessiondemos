package day3_InterfaceDemo;

public class EcommerceMainClass {

	public static void main(String[] args) {
		FlipKart flipkart = new FlipKart();
		String member=flipkart.memberDetails("Manzoor");
		System.out.println("member logged in is "+member);
		
		Ecommerce ecom= new FlipKart();
		ecom.memberDetails("Mehadi");
		
		ecom= new Amazon();
		int order=ecom.purchaseOrder(5);
		System.out.println("total orders placed are "+order);
	}
}
