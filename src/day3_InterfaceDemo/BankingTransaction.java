package day3_InterfaceDemo;

public interface BankingTransaction {

	public abstract String transactionDetails();
}
