package day3_InterfaceDemo;

public class FlipKart implements Ecommerce, BankingTransaction{

	@Override
	public String memberDetails(String memberName) {
		// TODO Auto-generated method stub
		return memberName;
	}

	@Override
	public int purchaseOrder(int noOfOrders) {
		int previousOrder=5;
		int totalNoOfOrders=previousOrder+noOfOrders;
		return totalNoOfOrders;
	}

	@Override
	public String transactionDetails() {
		String transactionMessage="Transaction Successful";
		return transactionMessage;
	}

}
