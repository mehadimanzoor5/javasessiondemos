package day2_Session1;

import java.util.Arrays;
import java.util.Scanner;

public class RuntimeArrayCreation {

	public static void main(String[] args) {
		int size;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter size of an array");
		size=sc.nextInt();
		int[] empId=new int[size];
		System.out.println("enter empId");
		for (int i=0;i<size;i++) {
			empId[i]=sc.nextInt();
		}		
		System.out.println(Arrays.toString(empId));
		int temp=empId[0];
		for(int i=1;i<empId.length;i++) {
			if (empId[i]>temp) {
				temp=empId[i];
			}
		}		
		System.out.println("the biggest number is "+temp);
	}
}