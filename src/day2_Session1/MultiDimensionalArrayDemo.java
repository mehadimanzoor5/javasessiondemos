package day2_Session1;

public class MultiDimensionalArrayDemo {

	public static void main(String[] args) {
		int[][] stuId= {{2,4,5},{6,2,8},{9,3,7}};
		
		System.out.println("total number of rows having data are "+stuId.length);
		for(int i=0;i<stuId.length;i++) {
			//System.out.println("total number of values present inside an "+i+"th row is "+stuId[i].length);
			for(int j=0;j<stuId[i].length;j++) {
				System.out.print(stuId[i][j]+"\t");
			}
			System.out.println();
		}

	}

}
