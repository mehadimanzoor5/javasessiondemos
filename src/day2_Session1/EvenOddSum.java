package day2_Session1;

import java.util.Arrays;
import java.util.Scanner;

public class EvenOddSum {

	public static void main(String[] args) {
		int size,evenSum=0,oddSum=0;
		Scanner sc = new Scanner(System.in);
		System.out.println("enter size of an array");
		size=sc.nextInt();
		int[] empId=new int[size];
		System.out.println("enter empId");
		for (int i=0;i<size;i++) {
			empId[i]=sc.nextInt();
		}		
		System.out.println(Arrays.toString(empId));
		for(int i=0;i<empId.length;i++) {
			if (empId[i]%2==0) {
				evenSum=evenSum+empId[i];
			}else
			{
				oddSum=oddSum+empId[i];
			}
		}
		
		System.out.println("sum of even number are "+evenSum);
		System.out.println("sum of odd number are "+oddSum);

	}

}
