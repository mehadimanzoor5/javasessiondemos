package day3_exceptionHandling;

import java.util.Scanner;

public class StudentDetails {

	public static void main(String[] args) {
		int[] stuId = new int[3];
		Scanner sc = new Scanner(System.in);
		System.out.println("enter studentid");
		try {
		for(int i=0;i<=stuId.length;i++) {
			stuId[i]=sc.nextInt();
		}
		}catch (ArrayIndexOutOfBoundsException ae) {
			System.out.println("number of array elements are execeeded please have a look");
		}finally
		{
			System.out.println("i will execute without any condition");
		}
	}

}
