package day1_Session2;

import java.util.Scanner;

public class ArithmeticOperationUsingSwitch {

	public static void main(String[] args) {
		int a, b;
		Scanner sc= new Scanner(System.in);
		System.out.println("enter the values of a and b");
		a=sc.nextInt();
		b=sc.nextInt();
		System.out.println(" 1.Add \n 2.Sub \n 3. Multiply \n 4. Divide \n Select your choice");
		int choice=sc.nextInt();
		switch (choice) {
		case 1: 
			System.out.println("add operation will trigger");
			System.out.println(a+b);
			break;
		case 2: 
			System.out.println("substraction operation will trigger");
			System.out.println(a-b);
			break;
		case 3: 
			System.out.println("Multiply operation will trigger");
			System.out.println(a*b);
			break;
		case 4:
			System.out.println("Division operation will trigger");
			System.out.println(a/b);
			break;
		default:
				System.out.println("invalid choice");
		}
		sc.close();

	}

}
