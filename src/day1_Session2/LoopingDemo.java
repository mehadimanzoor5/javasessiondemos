package day1_Session2;

public class LoopingDemo {

	public static void main(String[] args) {
		int a=21;
		
		while(a<20) {
			System.out.println(a);
			a++;
		}
		
		for(int i=6;i<5;i++) {
			System.out.println(i);
		}
		
		int b=21;
		do {
			System.out.println(b);
			b++;
		}while(b<20);
	}

}
