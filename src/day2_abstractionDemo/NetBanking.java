package day2_abstractionDemo;

public abstract class NetBanking {

	public abstract void smsService();

	public abstract void ministatement();
}
