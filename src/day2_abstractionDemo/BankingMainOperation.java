package day2_abstractionDemo;

public class BankingMainOperation {

	public static void main(String[] args) {
		BankingOperation bankOperation= new BankingOperation();
		bankOperation.bankName();
		bankOperation.smsService();
		int currentBalance=bankOperation.balanceEnquiry();
		System.out.println("current balance= "+currentBalance);
		currentBalance=bankOperation.cashWithdrawal(currentBalance, 500);
		System.out.println("current balance= "+currentBalance);
	}

}
