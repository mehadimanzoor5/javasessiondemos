package day2_abstractionDemo;

public class BankingOperation extends BankingService{
	int result;
	@Override
	public void smsService() {
		System.out.println("Welcome to Banking Service");		
	}
	@Override
	public int balanceEnquiry() {
		result=5000;
		return result;
	}
	@Override
	public int cashWithdrawal(int currentBalance, int amount) {
		result=currentBalance-amount;
		return result;
	}
	public void bankName() {
		System.out.println("ICICI Banking Operation");
	}
}
