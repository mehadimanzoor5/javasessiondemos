package day3_encapsulation;

import java.util.Scanner;

public class CompanyApplication {

	public static void main(String[] args) {
		EmployeeDetails empDetails= new EmployeeDetails();
		Scanner sc= new Scanner(System.in);
		System.out.println("please enter employee id");
		int empId=sc.nextInt();
		Scanner sc1= new Scanner(System.in);
		System.out.println("please enter employee name");
		String empName=sc1.nextLine();
		empDetails.setEmpId(empId);
		empDetails.setEmpName(empName);
		int id=empDetails.getEmpId();
		String name=empDetails.getEmpName();
		System.out.println("Employee Details are \n"+id+"\t"+name);
	}

}
