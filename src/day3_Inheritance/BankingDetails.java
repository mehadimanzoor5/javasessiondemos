package day3_Inheritance;

public class BankingDetails {

	public static void main(String[] args) {
		SBI sbi= new SBI();
		sbi.customerService();
		sbi.transactionLimit();
		int remainingTransaction=sbi.currentTransaction(2);
		System.out.println("Remaining transactions are "+remainingTransaction);
		
		Canara canara= new Canara();
		canara.welcomeMail();
		canara.call();
		
	}

}
