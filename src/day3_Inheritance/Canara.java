package day3_Inheritance;

public class Canara extends SBI{
	
	public void transactionLimit() {
		System.out.println("total number of transaction allowed in a month are 10");
	}

	public void call() {
		transactionLimit();
		super.transactionLimit();
	}
}
