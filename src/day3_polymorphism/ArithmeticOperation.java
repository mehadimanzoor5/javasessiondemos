package day3_polymorphism;

public class ArithmeticOperation {

	public static void main(String[] args) {
		MathematicalOperation math= new MathematicalOperation();
		math.add();
		math.add(6);
		math.add(3, 4);
		int result=math.add("Add", 6);
		System.out.println(result);
	}

}
