package day3_polymorphism;

public class MathematicalOperation {
	
	public void add() {
		System.out.println("you are into addition operation");
	}
	
	public void add(int a) {
		System.out.println("you have added only one input please pass another");
	}
	
	public void add (int a, int b) {
		int result=a+b;
		System.out.println("you have called addition operation without return type hence result is \n"+result);
	}
	
	public int add(String operation, int b) {
		int result=5+b;
		System.out.println("you have called addition operation with return type but with different argument \n hence result is ");
		return result;
	}

}
